<?php
namespace app\common\controller;

use org\Auth;
use think\Loader;
use think\Cache;
use think\Controller;
use think\Db;
use think\Session;
use app\common\model\Addons as AddonsModel;
/**
 * 后台公用基础控制器
 * Class AdminBase
 * @package app\common\controller
 */
class AdminBase extends Controller
{
    protected function _initialize()
    {
        parent::_initialize();
        
       
     
        
        $this->checkAuth();
        $this->getMenu();

        // 输出当前请求控制器（配合后台侧边菜单选中状态）
        $this->assign('controller', Loader::parseName($this->request->controller()));
    }

    /**
     * 权限检查
     * @return bool
     */
    protected function checkAuth()
    {

        if (!Session::has('admin_id')) {
            $this->redirect('admin/login/index');
        }

        $module     = $this->request->module();
        $controller = $this->request->controller();
        $action     = $this->request->action();

        // 排除权限
        $not_check = ['admin/Index/adminindex','admin/Index/deal_sql','admin/Public/tips', 'admin/AuthGroup/getjson', 'admin/System/clear', 'admin/System/ajax_mail_test', 'admin/System/doUploadPic', 'admin/Upload/upimage', 'admin/Upload/upfile', 'admin/Upload/umeditor_upimage', 'admin/Upload/layedit_upimage'];

        if (!in_array($module . '/' . $controller . '/' . $action, $not_check)) {
            $auth     = new Auth();
            $admin_id = Session::get('admin_id');
            if (!$auth->check($module . '/' . $controller . '/' . $action, $admin_id) && $admin_id != 1) {
            	return json(array('code' => 0, 'msg' => '没有权限'));
            	//$this->error('没有权限');
            }
        }
    }
  
    /**
     * 获取侧边栏菜单
     */
    protected function getMenu()
    {
        $menu     = [];
        $admin_id = Session::get('admin_id');
        $auth     = new Auth();

        $auth_rule_list = Db::name('auth_rule')->where('status', 1)->order(['sort' => 'DESC', 'id' => 'ASC'])->select();

        foreach ($auth_rule_list as $value) {
            if ($auth->check($value['name'], $admin_id) || $admin_id == 1) {
            	
            	$value['href']=url($value['name']);
            	
                $menu[] = $value;
                
                
            }
        }
        $addons        = new AddonsModel();
        $AdminList=$addons->getAdminList();
       if(!empty($AdminList)){
       	foreach($AdminList as $key=> $vo){
       	
       		$url='';
       		$url=url($vo['url']);
       		$adminliststr[$key]['href']=$url;
       		$adminliststr[$key]['title']=	$vo['title'];
       		$adminliststr[$key]['pid']=	104;
       		$adminliststr[$key]['id']='104'.$key;
       		$menu[] =$adminliststr[$key];
       	}
       }
       
        
       
        
        $menu = !empty($menu) ? array2tree($menu) : [];

        if(!empty($menu)){
        	
        }
        $this->assign('menu',  json_encode($menu));
      // return  json_encode($menu);
      //  return $menu;
        
        
    }
}