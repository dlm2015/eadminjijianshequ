<?php
// +----------------------------------------------------------------------
// | Author: Bigotry <3162875@qq.com>
// +----------------------------------------------------------------------

namespace app\common\behavior;

use think\Hook;
use think\Db;
use think\Cache;
/**
 * 初始化钩子信息行为
 */
class InitHook
{
	public function run(&$content){
		if(isset($_GET['m']) && $_GET['m'] === 'Install') return;
	
		$data = Cache::get('hooks');
		
		if(!$data){
			$hooks = Db::name('Hooks')->column('name,addons');
			
			foreach ($hooks as $key => $value) {
				if($value){
					$map['status']  =   1;
					$names          =   explode(',',$value);
					$map['name']    =   array('IN',$names);
					$data = Db::name('Addons')->where($map)->column('id,name');
					if($data){
						$addons = array_intersect($names, $data);
						Hook::add($key,array_map('get_addon_class',$addons));
					}
				}
			}
			Cache::set('hooks',Hook::get());
			 
		}else{
		
			Hook::import($data,false);
		}
	}
    /**
     * 行为入口
     */
/*     public function run()
    {
        
        $HookModel  = model(MODULE_COMMON_NAME.'/Hook');
        
        $AddonModel = model(MODULE_COMMON_NAME.'/Addon');
        
        // 获取所有配置信息
        $hook_list = $HookModel->column('name,addon_list');
        
        foreach ($hook_list as $k => $v) {

            if (empty($v)) {
                
                continue;
            }
                
            $where[DATA_STATUS] = DATA_NORMAL;
            $name_list = explode(',', $v);
            $where['name'] = ['in', $name_list];

            $data = $AddonModel->where($where)->column('id,name'); 

            !empty($data) && Hook::add($k, array_map('get_addon_class', array_intersect($name_list, $data)));
        }
    } */
}
