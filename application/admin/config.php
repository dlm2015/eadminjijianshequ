<?php
return [
    
	'template'=> [
    'view_suffix' => 'html',
	'view_depr'    => '_',
    ],
		// 'dispatch_success_tmpl'    => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',
	//	 'dispatch_error_tmpl'    => THINK_PATH . 'tpl' . DS . 'dispatch_jump.tpl',
		//默认错误跳转对应的模板文件
		'dispatch_error_tmpl' => 'public/tips',
		//默认成功跳转对应的模板文件
		'dispatch_success_tmpl' => 'public/tips',
		'url_route_on'  =>  false,
		// 默认控制器名
		'default_controller'     => 'Index',
		// 默认操作名
		'default_action'         => 'adminindex',
];
